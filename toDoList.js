// If an li element is clicked, toggle the class "done" on the <li>
const deleteElement = `<a class="delete">Delete</a>`;
const ul = document.getElementsByClassName("today-list")[0];
const addLink = document.querySelector("a.add-item");

document.body.addEventListener("click", function (event){
  const clickTarget = event.target;
  const listItem = clickTarget.closest("li");
  if (clickTarget.className === "delete") {
    listItem.remove();
  }
  
  });

// If a delete link is clicked, delete the li element / remove from the DOM
addLink.addEventListener("click", addListItem);

// If an 'Add' link is clicked, adds the item as a new list item with
// addListItem function has been started to help you get going!
// Make sure to add an event listener(s) to your new <li> (if needed)
function addListItem(e) {
  e.preventDefault();
  const input = this.parentNode.getElementsByTagNamne("input")[0];
  const text = input.value;
  const newListElem = document.createElement("li");
  const newTextContent = document.createElement("span");

  newTextContent.innerText = text;
  newListElem.appendChild(newTextContent);
  newListElem.insertAdjacentHTML('beforeend', deleteElement);

  ul.appendChild(newListElem);

};