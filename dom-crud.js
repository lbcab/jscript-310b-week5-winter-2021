// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
let atag = document.createElement("a");
atag.innerText = "Buy Now";
atag.setAttribute("id", "cta");
let mtag = document.getElementsByTagName("main")[0];
mtag.appendChild(atag);

// Access (read) the data-color attribute of the <img>,
// log to the console
let img1 = document.getElementsByTagName("img")[0];
let data = img1.dataset.color;
console.log(data);

// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
let childList = documemt.getElementsByTagName("ul")[0].children.item(2);
list1.classList = "hightlight";

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
let lpara = document.querySelector("main > p");
mtag.removeChild(lpara);