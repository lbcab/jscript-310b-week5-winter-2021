// Change the text of the "Seattle Weather" header to "February 10 Weather Forecast, Seattle"
const head1 = document.getElementById("weather-head");
head1.innerText = "February 10 Weather Forecast, Seattle";

// Change the styling of every element with class "sun" to set the color to "orange"
const class1 = document.getElementsByClassName("sun");
class1.style.color = "orange";

// Change the class of the second <li> from to "sun" to "cloudy"
const class2 = document.getElementsByClassName("sun");
class2[0].innerText = "cloudy"; 