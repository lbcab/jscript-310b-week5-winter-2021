// Attach one listener that will detect clicks on any of the <td>
// elements.  Should update that element's innerHTML to be the
// x, y coordinates of the mouse at the time of the click
const tdElements = document.getElementsByTagName("td");

console.log(tdElements);

for(let i = 0;i < tdElements.length; i++){
    tdElements[i].addEventListener("click", (event) => {
        const coordString = `x: ${event.clientX} | y: ${event.clientY}`;
        console.log(coordString);
        const currentCell = event.target;
        currentCell.innerText = coordString;
    })
}